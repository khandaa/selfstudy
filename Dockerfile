# Use the official Node.js image as the base image
FROM node:14-alpine

# Set the working directory
WORKDIR /app

# Copy the package.json and package-lock.json files to the working directory
COPY package*.json ./

# Install the dependencies
RUN npm install

# Copy the rest of the application code to the working directory
COPY . .

# Build the React app
RUN npm run build

# Install a simple static server to serve the React app
RUN npm install -g serve

# Expose port 3000
EXPOSE 3000

# Start the React app using the static server
CMD ["serve", "-s", "build", "-l", "3000"]
