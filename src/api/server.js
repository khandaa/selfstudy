const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');


const jwt = require('jsonwebtoken');
const cors = require('cors');

const app = express();
const PORT = process.env.PORT || 4000;
const SECRET_KEY = '123123'; // Use a secure key in production

app.use(bodyParser.json());

// Enable CORS for all routes
app.use(cors({
    origin: ['http://localhost:3000', 'http://anotherdomain.com']
}
));

app.post('/login', (req, res) => {
  const { email, password } = req.body;
    // console.log(email);
    // console.log(password);
  // Read users from the JSON file
  // Get the current directory
    const currentDirectory = __dirname;
    //console.log(currentDirectory+'/users.json');
  fs.readFile(currentDirectory+'/users.json', (err, data) => {
    if (err) {
      return res.status(500).json({ error: 'Unable to read json file. Internal server error' });
    }

    const users = JSON.parse(data).users;
    const user = users.find(u => u.email === email && u.password === password);
    //console.log(user);
    if (user) {
      // Generate a token
      const token = jwt.sign({ email: user.email }, SECRET_KEY, { expiresIn: '1h' });
      //console.log(token);
      return res.json({ token });
    } else {
      return res.status(401).json({ error: 'Invalid login credentials' });
    }
  });
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
