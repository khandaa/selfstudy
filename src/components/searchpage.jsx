import React from 'react';

const SearchPage = ({ onLogout }) => {
  return (
    <div className="container mt-5">
      <h2>Search Page</h2>
      <p>This is the search page.</p>
      <button className="btn btn-secondary" onClick={onLogout}>Logout</button>
    </div>
  );
};

export default SearchPage;
