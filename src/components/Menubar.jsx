import React from 'react';

// MenuBar component
const MenuBar = ({ activeMenu, setActiveMenu }) => {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <MenuItem title="My Questions" active={activeMenu === "My Questions"} onClick={() => setActiveMenu("My Questions")} />
            <MenuItem title="Create New Question" active={activeMenu === "Create New Question"} onClick={() => setActiveMenu("Create New Question")} />
            <MenuItem title="Modify" active={activeMenu === "Modify"} onClick={() => setActiveMenu("Modify")} />
            <MenuItem title="All Questions" active={activeMenu === "All Questions"} onClick={() => setActiveMenu("All Questions")} />
          </ul>
        </div>
      </div>
    </nav>
  );
};

// MenuItem component
const MenuItem = ({ title, active, onClick }) => {
  return (
    <li className="nav-item">
      <a className={`nav-link ${active ? "active" : ""}`} href="#" onClick={onClick}>{title}</a>
    </li>
  );
};

export default MenuBar;
